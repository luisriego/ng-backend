import { Component, OnInit, Output, Input, ViewChild, EventEmitter, ElementRef } from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styles: []
})
export class IncrementadorComponent implements OnInit {

  @ViewChild('txtProgreso') txtProgreso: ElementRef;

  @Input() progreso: number = 50;
  @Input() leyenda: string = 'Leyenda';

  @Output() cambioValor: EventEmitter<number> = new EventEmitter();

  constructor() {  }

  ngOnInit() {
  }

  alCambiar( nuevoValor: number ) {
    if (nuevoValor >= 100) {
      this.progreso = 100;
    }else if (nuevoValor <= 0) {
      this.progreso = 0;
    }else {
      this.progreso = nuevoValor;
    }

    this.txtProgreso.nativeElement.value = this.progreso;

    this.cambioValor.emit(this.progreso);
  }

  modificarProgreso( valor: number) {
    if (this.progreso <= 0 && valor < 0 ) {
      this.progreso = 0;
      return;
    }
    if (this.progreso >= 100 && valor > 0 ) {
      this.progreso = 100;
      return;
    }
    this.progreso += valor;
    this.cambioValor.emit(this.progreso);

    this.txtProgreso.nativeElement.focus();
  }

}
